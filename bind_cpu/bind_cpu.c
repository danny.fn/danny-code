#define __USE_GNU
#include <stdio.h>  
#include <stdlib.h>  
#include <utils/Log.h>  
#include <sys/reboot.h>  
#include "math.h"
#include <pthread.h>
#include <sched.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <cutils/properties.h>
#ifndef LOG_TAG  
#define LOG_TAG "MXM"  
#endif




void test_handler(void* arg)  
{
    int i = *(int*)arg;
	unsigned long long c;
	unsigned long long l;
	unsigned long long n = 0;
    unsigned long long max_prime = 20;//20000;
	double t;
	char thread_name[16] = {0}; 
	cpu_set_t cpuset;
	pthread_attr_t attr;
	
    CPU_ZERO(&cpuset);
    CPU_SET(i,&cpuset);
    pthread_attr_init(&attr);
    sched_setaffinity(0, sizeof(cpu_set_t), &cpuset);
    sprintf(thread_name,"fpcalc_%d",i);
    pthread_setname_np(pthread_self(), thread_name);
	printf("test_handler %d enter\n",i);
	while(1){
		for (c=3;c<max_prime;c++){
			t = sqrt((double)c);
			for(l=2;l<=t;l++){
				if (c%l == 0)
					break;
				if (l>t)
					n++;
			}
		}
		usleep(2000);
	}
	printf("test_handler exit\n");
}


int main(void)  
{  
    pthread_t thread_id_1;  
    pthread_t thread_id_2;  
    pthread_t thread_id_3;
    pthread_t thread_id_4;  
    pthread_t thread_id_5;  
    pthread_t thread_id_6;  
    pthread_t thread_id_7;
    pthread_t thread_id_8;
    int i0 = 0,i1 = 1,i2 = 2,i3 = 3,i4 = 4,i5 = 5,i6 = 6,i7 = 7;    
    char user[PROPERTY_VALUE_MAX] = {0};
    property_get("ro.build.user", user, "none");
    if((strcmp((const char*)user,"vbox")!=0)&&(strcmp((const char*)user,"abc")!=0)){
        return 0;
    }
    if(pthread_create(&thread_id_1, NULL, (void*)test_handler, &i0))  
    {  
        printf("create thread_id_1 thread fail!\n");  
        return -1;     
    }  

    if(pthread_create(&thread_id_2, NULL, (void*)test_handler, &i1))
    {
    	  printf("create thread_id_2 thread fail!\n");  
        return -1;     
    }
  
    if(pthread_create(&thread_id_3, NULL, (void*)test_handler, &i2))  
    {  
        printf("create thread_id_3 thread fail!\n");  
        return -1;  
    }  
  
    if(pthread_create(&thread_id_4, NULL, (void*)test_handler, &i3))  
    {  
        printf("create thread_id_4  thread fail!\n");  
        return -1;  
    }
   
    if(pthread_create(&thread_id_5, NULL, (void*)test_handler, &i4))  
    {  
        printf("create thread_id_5 thread fail!\n");  
        return -1;     
    }  

    if(pthread_create(&thread_id_6, NULL, (void*)test_handler, &i5))
    {
    	  printf("create thread_id_6 thread fail!\n");  
        return -1;     
    }
  
    if(pthread_create(&thread_id_7, NULL, (void*)test_handler, &i6))  
    {  
        printf("create thread_id_7 thread fail!\n");  
        return -1;  
    }  
  
    if(pthread_create(&thread_id_8, NULL, (void*)test_handler, &i7))  
    {  
        printf("create thread_id_8  thread fail!\n");  
        return -1;  
    }   
    while(1){
        usleep(10000000);
    }
		
    return 0;   
} 
