
LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_SRC_FILES:= innosz_input.c
LOCAL_MODULE:= innosz_input
LOCAL_LD_FLAGS := -Wl,--hash-style=sysv
LOCAL_LDFLAGS_arm := -Wl,--hash-style=sysv
LOCAL_LDFLAGS := -Wl,--hash-style=sysv
libsysv-hash-table-library_ldflags := Wl,-hash-style=sysv
#LOCAL_FORCE_STATIC_EXECUTABLE := true
#LOCAL_MODULE_PATH := $(TARGET_ROOT_OUT_SBIN)
#LOCAL_UNSTRIPPED_PATH := $(TARGET_ROOT_OUT_SBIN_UNSTRIPPED)
#LOCAL_STATIC_LIBRARIES := libcutils libc

LOCAL_SHARED_LIBRARIES := libc 

include $(BUILD_EXECUTABLE)

