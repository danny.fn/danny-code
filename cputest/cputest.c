#include <stdio.h>  
#include <stdlib.h>  
#include <utils/Log.h>  
#include <sys/reboot.h>  
#include "math.h"
 
#ifndef LOG_TAG  
#define LOG_TAG "MXM"  
#endif  

unsigned long long max_prime = 20000;

void test_handler()  
{
	unsigned long long c;
	unsigned long long l;
	unsigned long long n = 0;
	double t;
	printf("test_handler enter\n");
	while(1){
		for (c=3;c<max_prime;c++){
			t = sqrt((double)c);
			for(l=2;l<=t;l++){
				if (c%l == 0)
					break;
				if (l>t)
					n++;
			}
		}
	}
	printf("test_handler exit\n");
}
 
int main(void)  
{  
    pthread_t thread_id_1;  
    pthread_t thread_id_2;  
    pthread_t thread_id_3;
    pthread_t thread_id_4;  
    pthread_t thread_id_5;  
    pthread_t thread_id_6;  
    pthread_t thread_id_7;
    pthread_t thread_id_8;    

    if(pthread_create(&thread_id_1, NULL, (void*)test_handler, NULL))  
    {  
        printf("create thread_id_1 thread fail!\n");  
        return -1;     
    }  

    if(pthread_create(&thread_id_2, NULL, (void*)test_handler, NULL))
    {
    	  printf("create thread_id_2 thread fail!\n");  
        return -1;     
    }
  
    if(pthread_create(&thread_id_3, NULL, (void*)test_handler, NULL))  
    {  
        printf("create thread_id_3 thread fail!\n");  
        return -1;  
    }  
  
    if(pthread_create(&thread_id_4, NULL, (void*)test_handler, NULL))  
    {  
        printf("create thread_id_4  thread fail!\n");  
        return -1;  
    }
   
    if(pthread_create(&thread_id_5, NULL, (void*)test_handler, NULL))  
    {  
        printf("create thread_id_5 thread fail!\n");  
        return -1;     
    }  

    if(pthread_create(&thread_id_6, NULL, (void*)test_handler, NULL))
    {
    	  printf("create thread_id_6 thread fail!\n");  
        return -1;     
    }
  
    if(pthread_create(&thread_id_7, NULL, (void*)test_handler, NULL))  
    {  
        printf("create thread_id_7 thread fail!\n");  
        return -1;  
    }  
  
    if(pthread_create(&thread_id_8, NULL, (void*)test_handler, NULL))  
    {  
        printf("create thread_id_8  thread fail!\n");  
        return -1;  
    }   
    
		test_handler();
		
    return 0;   
} 
