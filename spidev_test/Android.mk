
LOCAL_PATH:= $(call my-dir)


include $(CLEAR_VARS)
LOCAL_SRC_FILES:= spidev_test.c
LOCAL_MODULE:= spidev_test

LOCAL_MODULE_TAG := optional
LOCAL_SHARED_LIBRARIES := libc libcutils libutils

include $(BUILD_EXECUTABLE)